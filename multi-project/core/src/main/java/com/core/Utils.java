package com.core;
import com.epam.rd.cre.utils.StringUtils;
public class Utils{


    public static boolean isAllPositiveNumbers(String ...str){
         
        boolean arePositives=true;
        for(String s:str)
        arePositives=arePositives&&StringUtils.isPositiveNumber(s);
        return arePositives;
    }
}