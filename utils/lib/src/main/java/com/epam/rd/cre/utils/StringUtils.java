package com.epam.rd.cre.utils;

public class StringUtils {


    public static void main(String[] args){
        
        System.out.println(StringUtils .isPositiveNumber("-1"));
    }

    public static  boolean isPositiveNumber(String str){
        boolean b = org.apache.commons.lang3.StringUtils.isNumeric(str) && !org.apache.commons.lang3.StringUtils.contains(str, "-");
        return b;
    }
}
